
rem = nil --[[
@echo off
texlua %0 %*
exit /b --]]

-- http://lua-users.org/wiki/StringIndexing
-- 补充 str[4] 这种写法
getmetatable('').__index = function(str,i)
  if type(i) == 'number' then
    return string.sub(str,i,i)
  else
    return string[i]
  end
end

-- 去掉开头和结尾的空白字符（包括空格, \t, \n, \r）
function string:trim(space)
   if not space then space = "%s" end
   local pattern = "^[" .. space .. "]*(.-)[" .. space .. "]*$"
   return self:match(pattern)
end

-- 字符串填充函数，在左侧加上空格
function string:lpad(size)
  local str = self
  while #str < size do str = " " .. str end
  return str
end

-- 字符串分割函数，其中的 sep 可以包含多个字符
-- includeEmpty 选项表示是否包含空白的子字符串
function string:split(sep, includeEmpty)
  local splits = {}
  if sep == nil then sep = " " end
  --print(self)
  local pattern = ".-" .. sep
  for str in string.gmatch(self .. sep, pattern) do
    str = str:sub(1, -#sep-1)
    --print(str)
    if str ~= "" or includeEmpty then
      table.insert(splits, str)
    end
  end
  --print(table.concat(splits, "\n"))
  return splits
end

-- 命令和环境的参数个数，不包括可选参数

cmds = {
  begin           = 1,
  clap            = 1,
  dfrac           = 2,
  diagbox         = 2,
  diagboxtwo      = 2,
  diagboxthree    = 3,
  dot             = 1,
  frac            = 2,
  --end           = 1,
  hat             = 1,
  hbox            = 1,
  hphantom        = 1,
  llap            = 1,
  mathclap        = 1,
  mathllap        = 1,
  mathop          = 1,
  mathord         = 1,
  mathrlap        = 1,
  mathrm          = 1,
  operatorname    = 1,
  overline        = 1,
  overrightarrow  = 1,
  overset         = 2,
  phantom         = 1,
  rlap            = 1,
  smash           = 1,
  sqrt            = 1,
  text            = 1,
  textrm          = 1,
  tfrac           = 2,
  underline       = 1,
  underrightarrow = 1,
  underset        = 2,
  vphantom        = 1,
  vec             = 1,
  widehat         = 1,
  xrightarrow     = 1,
}
cmds["end"] = 1

boxcmds = {
  clap    = 1,
  diagbox = 2,
  hbox    = 1,
  llap    = 1,
  rlap    = 1,
  text    = 1,
}

envs = {
  array    = 1,
  subarray = 1,
}

displayed = {
  "align", "align*", "gather", "gather*"
}

inline = {
  {"align",    "align"},
  {"align*",   "align*"},
  {"aligned",  "align*"},
  {"gather",   "gather"},
  {"gather*",  "gather*"},
  {"gathered", "gather*"},
}

formulas = {
  {"([^\\]?)(\\%()([^%$]-)([^\\])(\\%))", "\\(", "\\)"},
  {"(\n[^\n]-[^%s\t\n][^\n]-[^\\]?)(\\%[)([^%$]-)([^\\])(\\%])", "\\(", "\\)"},
  {"([^\\]?)(\\%[)([^%$]-)([^\\])(\\%])", "\\[", "\\]"},
}

mathsubs = {
  {"%{align%}", "%{aligned%}"},
  {"`", "'"},
  {"%^'", "'"},
  {"%^{''}", "''"},
  {"%^{'''}", "'''"},
  {" +'", "'"},
  {"%^{\\prime}", "'"},
  {"%^{\\prime ?\\prime ?}", "''"},
  {"%^{\\prime ?\\prime ?\\prime ?}", "'''"},
  {"%.%.%.", "\\cdots "},
  {"([^\\])(\\%a+) ([%(%[{])", "%1%2%3"},
  {"([^\\])(\\%a+) ([%)%]}])", "%1%2%3"},
  {"\\text{}", ""},
  {"\\text{([%a%d=%(%),%. ]-)}", "%1"},
  {"\\mathop{\\int\\!\\!\\!\\!\\!\\int\\mkern%-21mu \\bigcirc}", "\\oiint"},
  {"\\mathop{\\int \\int}", "\\iint"},
  {"\\mathop{\\int \\int \\int}", "\\iiint"},
  {"\\text{lim}", "\\lim"},
  {"\\mathop{\\lim}", "\\lim"},
  {"\\mathop{Lim}", "\\lim"},
  {"\\underset({[^{}]-}){\\lim}", "\\lim\\limits_%1"},
  {"\\underset({[^{}]-}){\\mathop{\\sum}}", "\\sum\\limits_%1"},
  {"\\underset({[^{}]-}){\\overset({[^{}]-}){\\mathop{\\sum}}}", "\\sum\\limits_%1^%2"},
  {"\\mathop{\\min}", "\\min"},
  {"\\mathop{\\max}", "\\max"},
  {"\\mkern 1mu", ""},
  {"\\bullet", "\\cdot"},
  {"\\cdot \\cdot \\cdot", "\\cdots"},
  {"\\overset{\\scriptscriptstyle\\rightharpoonup}", "\\vec"},
  {"\\nolimits", ""},
  {"\\bar +([xyzXYZ])", "\\overline{%1}"},
  {"\\left(| *_.-) *\\right%.", "\\big%1"},
  {"\\~", "\\sim "},
  {"\\leqslant", "\\le"},
  {"\\geqslant", "\\ge"},
  {"{%- (%d+)}", "{-%1}"},
  {" *%^ *", "%^"},
  {" *_ *", "_"},
  {"([%+%-]) +(\\infty)", "%1%2"},
  {"\\left%(([%a%d <=>%-]+)\\right%)", "(%1)"},
  {"\\left%[([%a%d <=>%-]+)\\right%]", "[%1]"},
  {"\\left\\{([%a%d <=>%-]+)\\right\\}", "\\{%1\\}"},
  {"\\left| *([%a%d <=>%-]-) *\\right|", "|%1|"},
  {"([%(%[{]) +", "%1"},
  {" +([%)%]}])", "%1"},
  {" +(\\})", "%1"},
  {"(\\cdot)([^ %a,\n])", "%1 %2"},
  {"(\\quad)([^ %a,\n])", "%1 %2"},
  {"[\t ]*\n", "\n"},
  {"([^\n& ]) +", "%1 "},
  {" *＼ *(\n\\end{align})", "%1"},
  {" *＼ *(\n\\end{align%*})", "%1"},
  {" *＼ *(\n\\end{aligned})", "%1"},
  {" *＼ *(\n\\end{gather})", "%1"},
  {" *＼ *(\n\\end{gather%*})", "%1"},
  {" *＼ *(\n\\end{gathered})", "%1"},
}

examsubs = {
  {"\\operatorname{cov}", "\\Cov"},
  {"\\operatorname{Cov}", "\\Cov"},
  {"\\operatorname{diag}", "\\diag"},
  {"\\operatorname{div}", "\\div"},
  {"\\operatorname{grad}", "\\grad"},
  {"\\operatorname{tr}", "\\tr"},
  {"(\\%a*int)\\limits_", "%1_"},
  {"\\limit_", "\\lim_"},
  {"\\lim\\limits_", "\\lim_"},
  {"\\sum\\limits_", "\\sum_"},
  {"\\prod\\limits_", "\\prod_"},
  {"{d}{d([rstuvxyzS])}", "{\\d}{\\d%1}"},
  {"{d(%b())}", "{\\d%1}"},
  {"([^\\])(dxdydz)", "%1\\dx\\dy\\dz"},
  {"([^\\])(dxdy)", "%1\\dx\\dy"},
  {"([^\\])(dxdz)", "%1\\dx\\dz"},
  {"([^\\])(dydx)", "%1\\dy\\dx"},
  {"([^\\])(dydz)", "%1\\dy\\dz"},
  {"([^\\])(dzdx)", "%1\\dz\\dx"},
  {"([^\\])(dzdy)", "%1\\dz\\dy"},
  {"([^\\])(dudv)", "%1\\du\\dv"},
  {"([^p\\])(d[rstuvxyzS])$", "%1\\%2"}, -- 不要影响偏微分
  {"([^p\\])(d[rstuvxyzS])([^%a])", "%1\\%2%3"}, -- 不要影响偏微分
  {"^(d[uvxyz])", "\\%1"},
  {"([^\\])(d\\rho)", "%1\\%2"},
  {"([^\\])(d\\theta)", "%1\\%2"},
  {"([^\\])(d\\sigma)", "%1\\%2"},
  {"([^\\])(d\\varphi)", "%1\\%2"},
  {"([^%a\\])(d%^[%dmnk]) *([xyzt])", "%1\\%2%3"},
  {"([^\\])(e^)", "%1\\%2"},
  {"\\text{d}", "\\d "},
  {"\\text{e}", "\\e "},
  {"{\\partial}", "{\\pd}"},
  {"\\partial (%a)([\\}])", "\\pd%1%2"}, --FIXME
  {"\\partial([ %^])", "\\pd%1"},
  {"\\&", "&"},
  {"\n(%(Ⅰ%))",  "\\par\n%1"},
  {"\n(%(Ⅱ%))",  "\\par\n%1"},
  {"\n(%(Ⅲ%))",  "\\par\n%1"},
}

textsubs = {
  {"__+", "  "},
  {"。", "．"},
  {"–", "-"},
  {"＝", "="},
  {"≤", "\\le "},
  {"≥", "\\ge "},
  {"〈", "<"},
  {"〉", ">"},
}

punctsubs = {
  --{"%$,([^%$])", "$，%1"},  -- FIXME
  --{"%$, ([^%$])", "$，%1"}, -- FIXME
  {"%$;",  "$；"},
  {"%$%.", "$．"},
  {",%$([\128-\255])",  "$，%1"},
  {";%$([\128-\255])",  "$；%1"},
  {"([^t])%.%$([\128-\255])", "%1$．%2"}, -- 略过 \right.$
  {", *([\128-\255])",  "，%1"},
  {"; *([\128-\255])",  "；%1"},
  {"%. *([\128-\255])", "．%1"},
  {"([\128-\255]), *",  "%1，"},
  {"([\128-\255]); *",  "%1；"},
  {"([\128-\255])%. *", "%1．"},
}

alignment = {}

alignment.empty = {
  "(\\begin{array}%b{}\n)([&{}%s]-)(\n\\end{array})",
}

alignment.bracketonesplit = {
  "\\left\\{ *\\begin{array}%b{}\n([^&]-)\n\\end{array} *\\right%."
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}",
}

alignment.brackettwosplit = {
  "\\left[%(%[] *"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}"
    .. " *\\right[%)%]]",
  "\\left[%(%[] *"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}"
    .. " *\\right[%)%]]",
  "\\left[%(%[] *"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}" .. "\\;\\;"
    .. "\\begin{array}%b{}\n([^&]-)\n\\end{array}"
    .. " *\\right[%)%]]",
}

alignment.matrixtoarray = {
  "(\\left\\{) *(\\begin{matrix}\n)(.-)(\n\\end{matrix}) *(\\right%.)",
  "(\\left\\{) *(\\begin{gathered}\n)(.-)(\n\\end{gathered}) *(\\right%.)",
  "(\\left\\{) *(\\begin{aligned}\n)(.-)(\n\\end{aligned}) *(\\right%.)",
  "(\\left[%(%[]) *(\\begin{matrix}\n)(.-)(\n\\end{matrix}) *(\\right[%)%]])",
  "(\\left[%(%[]) *(\\begin{gathered}\n)(.-)(\n\\end{gathered}) *(\\right[%)%]])",
}

alignment.gather = {
  "(\\begin{gathered}\n)(.-)(\n\\end{gathered})",
}

alignment.arrayadjustment = {
  "(\\begin{array}{.-}\n)(.-)(\n\\end{array})",
}

alignment.arraytomatrix = {
  "\\left(%() *\\begin{array}{(.-)}\n(.-)\n\\end{array} *\\right(%))",
  "\\left(%[) *\\begin{array}{(.-)}\n(.-)\n\\end{array} *\\right(%])",
  "\\left(|) *\\begin{array}{(.-)}\n(.-)\n\\end{array} *\\right(|)",
  "\\left(\\{) *\\begin{array}{(.-)}\n(.-)\n\\end{array} *\\right(%.)",
}

-- 是否在环境的参数里面
isInEnvArgument = false

function main()
  local input = arg[1]
  local output = arg[2] or "opt-" .. input
  print(input,output)
  local f = assert(io.open(input, "rb"))
  local text = f:read("*all")
  f:close()
  --print(#text)
  text = cleanup(text)
  f = io.open(output, "w")
  f:write(text)
  f:close()
end

function cleanup(text)
  text = text:gsub("\r\n", "\n"):gsub("\r", "\n")
  text = text:gsub("\\\\", "＼"):gsub("\\%$", "＄")
  for k, v in pairs(boxcmds) do
    --print(k, v)
    local pattern = "\\" .. k --.. "("
    for _ = 1, v do
      pattern = pattern .. "%b{}"
    end
    --pattern = pattern .. ")"
    --print(pattern)
    text = text:gsub(pattern, function(str)
      if not str:match("[\128-\255]") then
        print(str)
      end
      str = str:gsub("%$", "¥"):gsub("\\[%(%)]", "¥"):gsub("\\[%[%]]", "¥¥")
      --print(str)
      return str
    end)
  end
  text = GetDisplayedEnvs(text)
  text = undollar(text)
  for _, v in ipairs(formulas) do
    text = text:gsub(v[1], function(c1, c2, c3, c4, c5)
      --print(_, c1, c2, c3, c4, c5, "\n")
      return c1 .. v[2] .. optimize(c3 .. c4) .. v[3]
    end)
  end
  text = text:gsub("\\[%(%)%[%]]", "$")
  text = text:gsub("\n\t*( *)%$([^%$]-)%$,?%.?\n", "\n%1$$%2$$\n")
  text = SetDisplayedEnvs(text)
  text = text:gsub("¥", "$")
  text = text:gsub("＼", "\\\\"):gsub("＄", "\\$")
  --print(text)
  for _, v in ipairs(textsubs) do
    text = text:gsub(v[1], v[2])
  end
  for _, v in ipairs(punctsubs) do
    text = text:gsub(v[1], v[2])
  end
  return text
end

---------------------------------------------------
-- 将 $...$ 和 $$...$$ 全部改为 \(...\) 和 \[...\]
---------------------------------------------------
function undollar(text)
  local result = ""
  local dollar = 0
  local inmath = 0
  for i = 1, #text do
    local c = text[i]
    if c == "$" then
      dollar = dollar + 1
    else
      if dollar > 0 then
        ---- end math mode
        if inmath == 2 then
          result = result .. "\\]"
          inmath = 0
          dollar = dollar - 2
        elseif inmath == 1 then
          result = result .. "\\)"
          inmath = 0
          dollar = dollar - 1
        end
        ---- begin math mode
        if dollar > 0 then 
          dollar = dollar % 4
          if dollar == 2 then
            result = result .. "\\["
            inmath = 2
          else -- dollar == 1 or dollar == 3
            result = result .. "\\("
            inmath = 1
          end
        end
        dollar = 0
      end
      result = result .. c
    end
  end
  return result
end

function GetDisplayedEnvs(text)
  for i, v in ipairs(displayed) do
    v = v:gsub("%*", "%%*")
    local pattern = "\\begin{" .. v .. "}\n" .. ".-" .. "\n\\end{" .. v .. "}"
    --print(pattern)
    text = text:gsub(pattern, function(m)
      --print(m)
      if m:match("\\end{" .. v .. "}[^$]") then
        return
      else
        --print("\\[" .. m .. "\\]")
        return "\\[" .. m .. "\\]"
      end
    end)
  end
  return text
end

function SetDisplayedEnvs(text)
  for i, v in ipairs(inline) do
    v[1] = v[1]:gsub("%*", "%%*")
    local pattern = "%$%$\\begin{" .. v[1] .. "}\n" .. "(.-)"
                     .. "\n\\end{" .. v[1] .. "}%$%$"
    --print(pattern)
    text = text:gsub(pattern, function(m)
      --print(m)
      if m:match("\\end{" .. v[1] .. "}") then
        return
      else
        local out = "\\begin{" .. v[2] .. "}\n" .. m
                     .. "\n\\end{" .. v[2] .. "}"
        --print(out)
        return out
      end
    end)
  end
  return text
end

function optimize(mathin)
  mathout = GetCommands(mathin)
  for k, v in ipairs(mathsubs) do
    mathout = mathout:gsub(v[1], v[2])
  end
  for k, v in ipairs(examsubs) do
    mathout = mathout:gsub(v[1], v[2])
  end
  local count = 1
  while count > 0 do
    mathout, count = mathout:gsub("\\text{([^{}]+)}\\text{([^{}]+)}", "\\text{%1%2}")
  end
  mathout = TidyAlignment(mathout)
  return mathout:trim()
end

function GetCommands(fragment)
  -- 对 array 等环境的参数文本不作整理
  -- 以避免错误地将 *{20}{c} 的花括号去掉
  if isInEnvArgument then return fragment end
  local i, count = 0, 0
  local result, cname, space, ename, c = "", "", "", "", ""
  local pp, pi = "", -1
  while i < #fragment do
    i = i + 1
    c = fragment[i]
    if c == "\\" then
      cname, space = fragment:match("^(%a+)(%s*)", i+1)
      if cname == nil then
        result = result .. "\\" .. fragment[i+1]
        i = i + 1
      elseif cname == "begin" then
        i = i + #cname + #space
        local ename, space = fragment:match("^{(%a+%*?)}(%s*)", i+1)
        i = i + #ename + 2 + #space
        -- 获取该环境的参数个数
        count = envs[ename]
        if count then
          isInEnvArgument = true
          pp, pi = GetParameters(fragment, count, i, ename)
          result = result .. "\\begin{" .. ename .. "}" .. space .. pp
          i = pi
          isInEnvArgument = false
        else
          result = result .. "\\begin{" .. ename .. "}" .. space
        end
      else
        --print(cname .. space .. "]")
        i = i + #cname + #space
        count = cmds[cname]
        if count then
          pp, pi = GetParameters(fragment, count, i, cname)
          result = result .. "\\" .. cname .. pp
          i = pi
        else
          result = result .. "\\" .. cname .. space
        end
      end
    elseif c == "_" or c == "^" then
      pp, pi = GetParameters(fragment, 1, i, "_")
      result = result .. c .. pp
      i = pi
    elseif c == "{" then
      pp, pi = GetParameters(fragment, 1, i-1)
      pp = pp:sub(2,-2)
      if fragment[i-2]:match("%a") then
        pp = " " .. pp
      end
      if fragment[pi+1]:match("%a") then
        pp = pp .. " "
      end
      result = result .. pp
      i = pi
    else
      result = result .. c
    end
  end
  return result
end

function GetParameters(fragment, cnt, i, name)
  local parameters = ""
  local para, bare = "", ""
  ---- 一个 [***] 形式的可选参数
  if fragment[i+1] == "[" then
    para = fragment:match("^%[.-%]%s*", i+1)
    i = i + #para
    bare = GetCommands(para:trim():sub(2,-2))
    parameters = parameters .. "[" .. bare:trim() .. "]"
  end
  for cc = 1, cnt do
    para, bare = "", ""
    ---- 一个配对编组作为命令参数
    if fragment[i+1] == "{" then
      local balance, p = 0, ""
      repeat
        i = i + 1
        p = fragment[i]
        -- 忽略 \{ 和 \} 的配对
        if p == "\\" then
          i = i + 1
          p = p .. fragment[i]
        elseif p == "{" then
          balance = balance + 1
        elseif p == "}" then
          balance = balance - 1
        end
        para = para .. p
      until balance == 0 or i == #fragment
      bare = GetCommands(para:sub(2,-2))
    ---- 一个控制序列作为命令参数
    elseif fragment[i+1] == "\\" then
      bare = fragment:match("^%a+%s*", i+2)
      if bare then
        i = i + 1 + #bare
      else
      bare = fragment[i+2]
        i = i + 2
      end
      bare = "\\" .. bare
    ---- 一个普通字符作为命令参数
    else
      bare = fragment:match("^%s*[^%s]", i+1)
      if bare == nil then
        print(name, fragment)
      end
      i = i + #bare
      bare = bare[-1]
    end
    if name == "_" and #bare == 1 then
      parameters = parameters .. bare
    else
      parameters = parameters .. "{" .. bare:trim() .. "}"
    end
  end
  return parameters, i
end

function TidyAlignment(tex)
  -- 连续执行两遍，因为可能有嵌套的空环境
  for _, v in ipairs(alignment.empty) do
    tex = tex:gsub(v, RemoveEmptyEnvs)
  end
  for _, v in ipairs(alignment.empty) do
    tex = tex:gsub(v, RemoveEmptyEnvs)
  end
  for _, v in ipairs(alignment.bracketonesplit) do
    tex = tex:gsub(v, BracketOneSplit)
  end
  for _, v in ipairs(alignment.brackettwosplit) do
    tex = tex:gsub(v, BracketTwoSplit)
  end
  for _, v in ipairs(alignment.matrixtoarray) do
    tex = tex:gsub(v, MatrixToArray)
  end
  for _, v in ipairs(alignment.gather) do
    tex = tex:gsub(v, GatherArrays)
  end
  for _, v in ipairs(alignment.arrayadjustment) do
    tex = tex:gsub(v, ArrayAdjustment)
  end
  for _, v in ipairs(alignment.arraytomatrix) do
    tex = tex:gsub(v, ArrayToMatrix)
  end
  return tex
end

-----------------------------------------------------
-- b = \begin{***}{***}, e = \end{***}, arr = 内容
-----------------------------------------------------
function RemoveEmptyEnvs(b, arr, e)
  --print(b, arr, e)
  if arr:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return false
  end
  return ""
end

------------------------------------------------------
-- 识别由两个仅一列的 array 组成的分段函数，合并为一个
-- \left\{\begin{array}{} arr1 \end{array}\right.
--        \begin{array}{} arr2 \end{array}
------------------------------------------------------
function BracketOneSplit(arr1, arr2)
  print("-----------------\n" .. arr1 .. "\n" ..  arr2)
  if arr1:match("\\begin{") or arr2:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return false
  end
  local cells1 = arr1:split("＼")
  local cells2 = arr2:split("＼")
  if #cells1 ~= #cells2 then return end
  local cells = {}
  for i = 1, #cells1 do
    table.insert(cells, cells1[i]:trim() .. " & " .. cells2[i]:trim())
  end
  local out = table.concat(cells, " ＼\n")
  out = "\\left\\{\\begin{array}{*{20}{l}}\n"
          .. out .. "\n\\end{array}\\right."
  print(out)
  return out
end

------------------------------------------------------
-- 识别由多个仅一列的 array 组成的矩阵，合并为一个
-- \left\[\begin{array}{} arr1 \end{array}\;\;
--        \begin{array}{} arr2 \end{array}\;\;
--        \begin{array}{} arr3 \end{array}\right]
------------------------------------------------------
function BracketTwoSplit(...)
  print("-----------------\n", ...)
  local cols = {...}
  local count = -1
  for j, v in pairs(cols) do
    -- 里面有嵌套环境时不作处理
    if v:match("\\begin{") then return end
    cols[j] = v:split("＼")
    if j == 1 then
      count = #cols[j]
    elseif count ~= #cols[j] then
      -- 行数不同不作处理
      return
    end
  end
  local rows = {}
  for i = 1, count do
    local cells = {}
    for j = 1, #cols do
       table.insert(cells, cols[j][i]:trim())
    end
    table.insert(rows, table.concat(cells, " & "))
  end
  local out = table.concat(rows, " ＼\n")
  out = "\\left(\\begin{array}{*{20}{c}}\n"
          .. out .. "\n\\end{array}\\right)"
  print(out)
  return out
end

--------------------------------------------------------------------
-- l = \left*, b = \begin{***}, arr = 内容, e = \end{***}, r=\right*
-- 将用单侧花括号或双侧括号包含的 gathered/matrix 改为 array 环境
--------------------------------------------------------------------
function MatrixToArray(l, b, arr, e, r)
  --print(l, b, arr, e, r)
  if arr:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return false
  end
  local splits = arr:split("＼")
  for k, v in ipairs(splits) do
    v = v:trim()
    v = v:gsub("^\\hfill *", ""):gsub(" *\\hfill$", "")
    splits[k] = v
  end
  local out = table.concat(splits, " ＼\n")  
  out = l .. "\\begin{array}{*{20}{c}}\n" .. out .. "\n\\end{array}" .. r
  return out
end

---------------------------------------------
-- b = \begin{***}, e = \end{***}, arr = 内容
---------------------------------------------
function GatherArrays(b, arr, e)
  --print(b, arr, e)
  if arr:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return false
  end
  local splits = arr:split("＼")
  local templ = ""
  local spaces = ""
  local equals = ""
  for k, v in ipairs(splits) do
    v = v:trim()
    local t = ""
    if v:match("\\hfill$") then
      t = "l"
    elseif v:match("^\\hfill") then
      t = "r"
    else
      t = "c"
    end
    templ = templ .. t
    v = v:gsub("^\\hfill *", ""):gsub(" *\\hfill$", "")
    splits[k] = v
    --print(v)
    local s = ""
    if v:match("\\;\\;.+\\;\\;") then
      s = "Y"
    elseif v:match("\\quad.+\\quad") then
      s = "Y"
    else
      s = "N"
    end
    spaces = spaces .. s
    local e = ""
    if v:match("^[^=]+=[ %-%a%d,%.]+$") then
      e = "Y"
    else
      e = "N"
    end
    equals = equals .. e
  end
  local out = ""
  local tblRows = {}
  if templ:match("^l+$") and spaces:match("^Y+$") then
    -- 识别出可能的阵列
    --print("=== array found ====\n" .. table.concat(splits, "\n"))
    local cols = nil
    for _, v in ipairs(splits) do
      local tblCells = {}
      v = v:gsub("\\;\\;", "\\quad")
      tblCells = v:split("\\quad", false)
      if not cols then
        cols = #tblCells
      elseif cols ~= #tblCells then
        -- 各行的列数不同，识别阵列失败
        return false
      end
      for kk, vv in ipairs(tblCells) do
        vv = vv:trim():gsub("^\\;", ""):gsub("\\;$", "")
        tblCells[kk] = vv
      end
      table.insert(tblRows, "  " .. table.concat(tblCells, " & "))
    end
    out = table.concat(tblRows, " ＼\n")
    out = "\\begin{array}{*{20}{l}}\n" .. out .. "\n\\end{array}"
    --print(out)
    return out
  elseif templ:match("^l+$") and equals:match("^Y+$") then
    -- 识别出可能的方程组
    --print("=== equations found ====\n" .. table.concat(splits, "\n"))
    out = table.concat(splits, " ＼\n")
    out = "\\begin{array}{*{20}{l}}\n" .. out .. "\n\\end{array}"
    --print(out)
    return out
  end
  return false
end

--------------------------------------------------
-- b = \begin{***}{***}, e = \end{***}, arr = 内容
--------------------------------------------------
function ArrayAdjustment(b, arr, e)
  --print(b, arr, e)
  if arr:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return b .. arr .. e
  end
  local hasVline = false
  if arr:match("\\vline") then
    hasVline = true
  end
  local lines = {}
  for s in arr:gmatch("[^\r\n]+") do
    if s:match("＼ *[^ %[]") then
      -- 若在一文本行里包含多个表格行，则不作处理
      return
    end
    s = s:gsub("\\vline%s*&%s*＼", " ＼ & \\vline ")
         :gsub("\\vline%s*&%s*$", "& \\vline ")
    table.insert(lines, s)
    --print(s)
  end 
  local arrlines = {}
  local cellnum = 0
  local template = ""
  for _, v in ipairs(lines) do
    local cells = {}
    local templ = ""
    ---- 避免多列混在一起，导致下面的 gmatch 无法分开
    v = v:gsub("&&", "& &"):gsub("&&", "& &")
    for c in v:gmatch("[^&]+") do
      c = c:trim():gsub("^%- +", "-")
      if c == "\\vline" then
        templ = templ .. "|"
      else
        templ = templ .. "c"
        table.insert(cells, c)
      end
    end
    if #templ > #template then template = templ end
    if #cells > cellnum then cellnum = #cells end
    table.insert(arrlines, cells)
  end
  --print(template)
  --print("-------- " .. #lines .. " " .. cellnum .. " --------")
  for i = 1, cellnum do
    local size = 0
    for j = 1, #arrlines do
      --print(arrlines[j][i])
      if arrlines[j][i] ~= "\\hline" then
        if arrlines[j][i] == nil then
          arrlines[j][i] = ""
        else
          size = math.max(#arrlines[j][i], size)
        end
      end
    end
    if hasVline and i ==1 and arrlines[1][1] == "\\hline" then
     arrlines[2][1] = arrlines[2][1]
       :gsub("(.-)\\backslash *(.-)\\backslash *(.+)", "\\diagboxthree{%1}{%2}{%3}")
       :gsub("(.-)\\backslash *(.+)", "\\diagboxtwo{%1}{%2}")
    end
    for j = 1, #arrlines do
      if arrlines[j][i] ~= "\\hline" then
        ---- string.format 不支持太长的字符串
        --arrlines[j][i] = string.format("%" .. size .. "s", arrlines[j][i])
        if i < cellnum then
          -- 最后一列左对齐
          arrlines[j][i] = arrlines[j][i]:lpad(size)
        end
      end
    end
  end
  local out = ""
  if #arrlines == 1 and #arrlines[1] == 1 then
    print("Only One Cell!", arr)
    return arrlines[1][1]
  end
  for j = 1, #arrlines do
    --print(arrlines[j][1])
    if arrlines[j][1] == "\\hline" then
      arrlines[j] = "\\hline"
    else
      arrlines[j] = "  " .. table.concat(arrlines[j], " & ")
    end
  end
  out = table.concat(arrlines, "\n")
  if out:match("\\hline$") and not out:match("＼\n\\hline$") then
    --print(out)
    out = out:gsub("%s*\n\\hline$", " ＼\n\\hline")
  end
  if hasVline then
    b = b:gsub("%*{20}{c}", template)
  end
  out = b .. out .. e
  -- print(out)
  return out
end

-----------------------------------------------------
-- l = 左定界符, t = 阵列模板, mat = 内容, r=右定界符
-----------------------------------------------------
function ArrayToMatrix(l, t, mat, r)
  --print(l, t, mat, r)
  if mat:match("\\begin{") then
    -- 里面有嵌套环境时不作处理
    return false
  end
  local env = ""
  if l == "(" and r == ")" then
    env = "pmatrix"
  elseif l == "[" and r == "]" then
    env = "pmatrix"
  elseif l == "|" and r == "|" then
    env = "vmatrix"
  elseif l == "\\{" and r == "." then
    env = "cases"
  else
    error("Unknown Delimeters!")
    return false
  end 
  return "\\begin{" .. env .. "}\n" .. mat .. "\n\\end{" .. env .. "}"
end

main()
